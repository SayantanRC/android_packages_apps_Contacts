/*
 * Copyright (C) 2019-2023  E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.android.contacts.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

public class GroupMembershipTools {

    public boolean isTypeGroupMembership(String mimeType) {
        return ContactsContract.CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE.equals(mimeType);
    }

    public String getGroupTitle(Context context, long groupId) {
        final Uri groupUri = ContactsContract.Groups.CONTENT_URI;

        final String[] projection = new String[]{
                ContactsContract.Groups._ID,
                ContactsContract.Groups.TITLE,
        };
        final String selection = ContactsContract.Groups.AUTO_ADD + " = ? AND " + ContactsContract.Groups.FAVORITES + " = ?";
        final String[] selectionArgs = new String[]{"0", "0"};

        try {
            Cursor groupCursor = context.getContentResolver().query(groupUri, projection, selection, selectionArgs, null, null);
            if (groupCursor.moveToFirst()) {
                do {
                    final int groupIdColumnIndex = groupCursor.getColumnIndex(ContactsContract.Groups._ID);
                    final int groupTitleColumnIndex = groupCursor.getColumnIndex(ContactsContract.Groups.TITLE);
                    if (groupIdColumnIndex == -1 || groupTitleColumnIndex == -1) {
                        groupCursor.close();
                        return null;
                    }
                    final long id = groupCursor.getLong(groupIdColumnIndex);
                    final String title = groupCursor.getString(groupTitleColumnIndex);
                    if (groupId == id) {
                        groupCursor.close();
                        return title;
                    }
                } while (groupCursor.moveToNext());
            }
            groupCursor.close();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
