# Instructions to make the project compilable from Android Studio

1. Clone this repo from Android Studio
2. Create an local.properties file (if not already made) to point to the Android Sdk on your device.
   ```
   sdk.dir=/home/<user>/Android/Sdk
   ```
3. Get the dependencies from the [below dependencies section](#dependencies).
4. Make the necessary [code changes as below](#code-changes)
5. Gradle sync the project.
6. Finally the project should now be compilable directly from Android Studio.

## Dependencies

⚠⚠⚠ Make sure to not commit these changes ⚠⚠⚠

Get the below dependencies and modify them

1. **PhoneCommon**: https://android.googlesource.com/platform/packages/apps/PhoneCommon/  
   Modifications after cloning:
   - build.gradle - add the line under `android` block.
     ```
     compileSdkVersion rootProject.ext.compileSdkVersion
     ```
2. **Android common (ex/common)**: https://android.googlesource.com/platform/frameworks/ex/  
   Modifications after cloning  
   (to be done under ex/common, ignore all other directories like camera2, photoviewer):
    - build.gradle - add the line under `android` block.
       ```
       compileSdkVersion rootProject.ext.compileSdkVersion
       ```
    - build.gradle - comment out `logtags` related lines of code.
       ```
       //apply plugin: 'logtags'
       ```
       ```
       //            logtags.srcDirs = ['java']
       ```
3. **vcard**: https://gitlab.e.foundation/e/os/vcard  
   Modifications after cloning:
    - build.gradle - change line 
       `compile getAndroidPrebuilt('9')` to `implementation getAndroidPrebuilt('9')`
    - build.gradle - add the following block above "dependencies" block
       ```
       // https://cs.android.com/android/platform/superproject/+/master:external/protobuf/build.gradle;drc=9a50fbff50f6963e449e12c611e96f20f9576e79;l=138
       // Libs from: https://github.com/Sable/android-platforms
       def getAndroidPrebuilt = { apiLevel ->
           files("android-${apiLevel}.jar")  
       }
       ```
    - Open github repo on browser - https://github.com/Sable/android-platforms.  
      Under "android-9" download the "android.jar" file, rename it to "android-9.jar".  
      Then move it inside the module (directly under vcard directory).
    - In Java, under com/android/vcard/exception, open "JapaneseUtils".  
      Search for the lines having "VisibleForTesting". If present, comment them out.

## Code changes

⚠⚠⚠ Make sure to not commit these changes ⚠⚠⚠

- `res/values/lineage_strings.xml`: Comment out the string "menu_emergency_information_txt".
- `src/com/android/contacts/activities/ContactSelectionActivity.java`: Comment out the line:
   ```
   getWindow().addSystemFlags(android.view.WindowManager.LayoutParams
               .SYSTEM_FLAG_HIDE_NON_SYSTEM_OVERLAY_WINDOWS);
   ```
- `src/com/android/contacts/vcard/ImportVCardActivity.java`: Comment out the same lines as in the above file.
- `src/com/android/contacts/vcard/SelectAccountActivity.java`: Comment out the same lines as in the above file.